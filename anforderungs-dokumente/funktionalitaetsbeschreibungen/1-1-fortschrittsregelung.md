# Benötigte Daten
- Prüfungsordnung
- Bestandene Module

# Funktionalität
1. PO prüfen
2. Module sammeln
3. Abgeschlossene Semester prüfen
4. Nicht verfügbare Moule filtern (z.B. alles ab dem vierten, sofern Mathe 1 nicht bestanden)
5. Liste an verfügbaren Modulen zurückgeben

# Anforderung
Das Modul dient als austauschbarer Service und muss daher jederzeit einfach zu ersetzen sein.