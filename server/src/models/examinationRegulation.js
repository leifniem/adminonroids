import mongoose from 'mongoose'
let examRegulation = mongoose.Schema({
	// Bezeichnung der PO (z.B. 2013)
	name: {
		type: Number,
		required: true,
		unique: true,
		min: 2013,
		max: 3000
	},
	// Regelstudienzeit
	regularDestinedSemesters: {
		type: Number,
		required: true,
		min: 4,
		max: 10
	},
	// Module in dieser Prüfungsordnung
	subjects: [{
		id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Subject'
		},
		// ordnr
		courseNumberInExamReg: {
			type: Number,
			required: true,
			unique: true,
			min: 10000,
			max: 99999
		},
		// Semester, in dem das Modul stattfindet
		destinedForSemester: {
			type: Number,
			required: true,
			min: 0,
			max: 10
		},
		// Vorgesehene CP für dieses Modul
		creditPoints: {
			type: Number,
			required: true
		},
		// Falls Modulname überschrieben werden soll
		titleOverride: String
	}]
})

export default mongoose.model('ExamReg', examRegulation)
