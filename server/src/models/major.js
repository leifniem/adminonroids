import mongoose from 'mongoose'

let major = mongoose.Schema({
	// Studiengang (z.B. Medieninformatik)
	name: {
		type: String,
		required: true,
		unique: true
	},
	// Dreistellige Abkürzung (z.B. IDB)
	abbreviation: {
		type: String,
		required: true,
		unique: true,
		minlength: 3,
		maxlength: 3
	},
	// Vorhandene Prüfungsordnungen für diesen Studiengang
	examRegulations: [mongoose.Schema.Types.ObjectId]
})

export default mongoose.model('Major', major)
