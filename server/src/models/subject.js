import mongoose from 'mongoose'

let subject = mongoose.Schema({
	// Modul Abkürzung (z.B. AFS)
	titleAbbreviation: {
		type: String,
		maxlength: 20,
		required: true
	},
	// Modulbezeichnung
	title: String,
	// Im aktuellen Semester angeboten?
	currentlyOffered: Boolean,
	// Lehrveranstaltungsnummer
	courseNumber: {
		type: Number,
		min: 1,
		max: 9999,
		required: true,
		unique: true
	},
	// Art des Moduls (Vorlesung, Praktikum, Übung, Tutorium)
	moduleType: {
		type: String,
		enum: ['V', 'P', 'Ü', 'T'],
		required: true
	},
	// Art der Leistung (Prüfungsleistung etc.)
	achievementType: {
		type: String,
		enum: ['PL', 'MO', 'SL', 'LN', 'VL'],
		required: true
	},
	// Zeitplan (der einzelnen Veranstaltungen) für dieses Modul
	schedule: [{
		// Wochentag
		weekday: {
			type: String,
			required: true
		},
		// Uhrzeit Beginn
		start: {
			type: String,
			required: true
		},
		// Uhrzeit Ende
		end: {
			type: String,
			required: true
		},
		// Gruppe (z.B. Praktikumsgruppe A, B, C etc.)
		group: String,
		// Raum (z.B. D12)
		room: {
			type: String,
			required: true
		},
		// Vortragender
		lecturer: {
			type: String,
			required: true
		},
		// Liste der Teilnehmer
		enrolledStudents: [{
			student: {
				type: mongoose.Schema.Types.ObjectId
			}
		}],
		// Maximale Teilnehmerzahl
		maxParticipants: {
			type: Number,
			required: true
		}
	}]
})

export default mongoose.model('Subject', subject)
