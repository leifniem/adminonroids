// // Needed for loading the models
// import fs from 'fs'

// // require.context('./', true, /\.js$/)

// // scan current folder and only load js files nor named index
// // into the model object above with their defined name
// function walk (folder, mongoose) {
// 	fs.readdirSync(folder).filter(function (file) {
// 		return (file.indexOf('.js') > -1) && (file !== 'index.js')
// 	}).forEach(function (file) {
// 		require(folder + '/' + file)(mongoose)
// 	})

// 	fs.readdirSync(folder).filter(function (file) {
// 		return (fs.lstatSync(folder + '/' + file).isDirectory())
// 	}).forEach(function (ele) { walk(folder + '/' + ele, mongoose) })
// }

// module.exports = function (mongoose, db) {
// 	walk(__dirname, mongoose, db)
// }

import major from './major'
import student from './student'
import subject from './subject'
import examReg from './examinationRegulation'

export default {
	major: major,
	student: student,
	subject: subject,
	examReg: examReg
}
