import mongoose from 'mongoose'

let student = mongoose.Schema({
	// Matrikelnummer des Studenten
	matriculationNumber: {
		type: Number,
		required: true,
		unique: true,
		min: 100000,
		max: 9999999
	},
	// Vorname (beinhaltet auch weitere Vornamen)
	firstName: {
		type: String,
		required: true,
		validate: {
			validator: function (v) {
				/^[a-zA-Z- äöüÄÖÜß]+$/.test(v)
			},
			message: 'Firstname contains invalid characters.'
		}
	},
	// Nachname
	lastName: {
		type: String,
		required: true,
		validate: {
			validator: function (v) {
				/^[a-zA-Z- äöüÄÖÜß]+$/.test(v)
			},
			message: 'Lastname contains invalid characters.'
		}
	},
	// Geschlecht
	sex: {
		type: String,
		required: true,
		enum: ['M', 'W']
	},
	// Status des Studenten (möglicherweise eingeschrieben, rückgemeldet etc.)
	status: {
		type: String,
		required: true,
		enum: ['E', 'N', 'R']
	},
	// Niedrigstes unfertiges Semester
	lowestUnfinishedSemester: {
		type: Number,
		required: true,
		min: 1,
		max: 6
	},
	// Fachsemester im Studiengang (wie lange ist Student eingeschrieben)
	semestersInMajor: {
		type: Number,
		required: true,
		min: 1,
		max: 99
	},
	// Studiengang
	major: {
		type: String,
		required: true,
		validate: {
			validator: function (v) {
				/^[A-Z]{3}$/.test(v)
			}
		}
	},
	// Prüfungsordnung
	examReg: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'ExamReg',
		required: true
	},
	creditPoints: Number,
	// Schon bestandene Module
	passedModules: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Subject'
		}
	],
	// eingeschriebene Module
	enrolledToModules: [{
		subject: {
			type: mongoose.Schema.Types.ObjectId
		},
		course: {
			type: mongoose.Schema.Types.ObjectId
		}
	}],
	username: {
		type: String
	},
	password: {
		type: String,
		required: true
	},
	email: {
		type: String
	}
},
{
	toObject: { virtuals: true },
	toJSON: { virtuals: true }
})

student.pre('save', function (next) {
	// generate username
	let first = this.firstName.substr(0, 2)
	let last = this.lastName
	while (last.length < 6) {
		last += 'x'
	}
	last = last.substr(0, 6)
	this.username = (first + last).toLowerCase()

	// generate email
	let secondName = ''
	if (this.firstName.includes(' ')) {
		secondName = this.firstName.split(' ')[1].substr(0, 1) + '.'
	}
	this.email = `${this.firstName.split(' ')[0]}.${secondName}${this.lastName}@student.hs-rm.de`.toLowerCase()

	next()
})

export default mongoose.model('Student', student)
