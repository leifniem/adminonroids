module.exports = function (dataDTO, mongoose) {
	const Subject = mongoose.model('Subject')

	let studentId = dataDTO.userId
	let subjectId = dataDTO.subjectId
	let group = dataDTO.group

	Subject.findOneAndUpdate(
		{
			_id: subjectId,
			schedule: { $elemMatch: { group: group } }
		},
		{
			$pull: { 'schedule.$.enrolledStudents': { student: studentId } }
		},
		(err, updated) => {
			if (err) console.error(err)
		}
	)
}
