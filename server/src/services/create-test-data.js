import { series } from 'async'
import bcrypt from 'bcrypt-nodejs'

// Erzeuge Testdaten (POs, Student, Module und deren Schedule)
export default async function (mongoose) {
	const Subject = mongoose.model('Subject')
	const Student = mongoose.model('Student')
	const ExamReg = mongoose.model('ExamReg')

	Student.remove({}, (err) => {
		if (err) console.error(err)
	})

	let testStud = {
		matriculationNumber: 666666,
		firstName: 'Max',
		lastName: 'Staigerung',
		sex: 'M',
		status: 'E',
		lowestUnfinishedSemester: 3,
		semestersInMajor: 5,
		major: 'IDB',
		password: bcrypt.hashSync('bla'),
		passedModules: [ /* cgVL._id */]
	}

	await series([
		(callback) => {
			ExamReg.findOne({ name: 2015 }).exec((err, po15) => {
				console.log(po15)
				if (err) callback(err)
				testStud['examReg'] = po15._id
				callback()
			})
		},
		(callback) => {
			Subject.find({
				titleAbbreviation: {
					$in: [
						'EinfMedInf',
						'EinfMdInfP',
						'Programm1',
						'Program1Pr',
						'EinfGestal',
						// 'Analysis',
						'Grdl BWL'
						// 'Algor+DS',
						// 'Algor+DSPr',
						// 'Auszeichsp',
						// 'Programm2',
						// 'Program2Pr',
						// 'Gest elMed',
						// 'LinAlgebra',
						// 'RechtInfor',
						// 'Aut+FormSp',
						// 'Datenbsyst',
						// 'EwiaBenObf',
						// 'EwiaBnObfP',
						// 'Animation',
						// 'Web-basAnw'
					]
				}
			},
			'_id')
				.exec((err, found) => {
					if (err) console.error(err)
					// console.log(found)
					found.forEach((element) => {
						testStud.passedModules.push(element)
					})
					callback()
				})
		},
		(callback) => {
			let stud = new Student(testStud)
			stud.save((err) => {
				if (err) console.error(err)
			})
			callback()
		}
	], (err, values) => {
		if (err) {
			console.error(err)
		} else {
			console.log(values)
		}
	})

	Subject.findOneAndUpdate({ courseNumber: 3111 }, {
		$push: {
			schedule: {
				weekday: 'Monday',
				start: '14:15',
				end: '15:45',
				group: 'V',
				room: 'D11',
				lecturer: 'Wz',
				enrolledStudents: [],
				maxParticipants: 100
			}
		}
	}, (err, subject) => { if (err) console.error(err) })

	Subject.findOneAndUpdate({ courseNumber: 2241 }, {
		$push: {
			schedule: {
				weekday: 'Monday',
				start: '10:00',
				end: '11:30',
				group: 'V',
				room: 'D11',
				lecturer: 'Pd',
				enrolledStudents: [],
				maxParticipants: 100
			}
		}
	}, (err, subject) => { if (err) console.error(err) })

	Subject.findOneAndUpdate({ courseNumber: 2242 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Monday',
					start: '11:45',
					end: '13:15',
					group: 'A',
					room: 'D15',
					lecturer: 'Pd',
					enrolledStudents: [],
					maxParticipants: 25
				},
				{
					weekday: 'Monday',
					start: '14:15',
					end: '15:45',
					group: 'B',
					room: 'D15',
					lecturer: 'Pd',
					enrolledStudents: [],
					maxParticipants: 25
				},
				{
					weekday: 'Monday',
					start: '16:00',
					end: '17:30',
					group: 'C',
					room: 'D15',
					lecturer: 'Pd',
					enrolledStudents: [],
					maxParticipants: 25
				},
				{
					weekday: 'Tueday',
					start: '10:00',
					end: '11:30',
					group: 'D',
					room: 'D15',
					lecturer: 'Pd',
					enrolledStudents: [],
					maxParticipants: 25
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2122 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Tuesday',
					start: '10:00',
					end: '11:30',
					group: 'C',
					room: 'D13',
					lecturer: 'Wl',
					enrolledStudents: [],
					maxParticipants: 25
				},
				{
					weekday: 'Tuesday',
					start: '11:45',
					end: '13:15',
					group: 'D',
					room: 'D13',
					lecturer: 'Wl',
					enrolledStudents: [],
					maxParticipants: 25
				},
				{
					weekday: 'Wednesday',
					start: '16:00',
					end: '17:30',
					group: 'B',
					room: 'D13',
					lecturer: 'Cv',
					enrolledStudents: [],
					maxParticipants: 25
				},
				{
					weekday: 'Wednesday',
					start: '17:45',
					end: '19:15',
					group: 'A',
					room: 'D13',
					lecturer: 'Cv',
					enrolledStudents: [],
					maxParticipants: 25
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2351 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Wednesday',
					start: '08:15',
					end: '09:45',
					group: 'V',
					room: 'D11',
					lecturer: 'Rc',
					enrolledStudents: [],
					maxParticipants: 100
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2352 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Wednesday',
					start: '10:00',
					end: '11:30',
					group: 'I',
					room: 'D11',
					lecturer: 'Rc',
					enrolledStudents: [],
					maxParticipants: 50
				},
				{
					weekday: 'Wednesday',
					start: '11:45',
					end: '13:15',
					group: 'II',
					room: 'D11',
					lecturer: 'Rc',
					enrolledStudents: [],
					maxParticipants: 50
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2112 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Wednesday',
					start: '10:00',
					end: '11:30',
					group: 'E',
					room: 'D13',
					lecturer: 'Sk',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Friday',
					start: '10:00',
					end: '11:30',
					group: 'A',
					room: 'D12',
					lecturer: 'Sk',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Friday',
					start: '10:00',
					end: '11:30',
					group: 'C',
					room: 'D13',
					lecturer: 'Mq',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Friday',
					start: '11:45',
					end: '13:15',
					group: 'B',
					room: 'D12',
					lecturer: 'Sk',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Friday',
					start: '11:45',
					end: '13:15',
					group: 'D',
					room: 'D13',
					lecturer: 'Mq',
					enrolledStudents: [],
					maxParticipants: 20
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2121 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Wednesday',
					start: '14:15',
					end: '15:45',
					group: 'V',
					room: 'D11',
					lecturer: 'Cv',
					enrolledStudents: [],
					maxParticipants: 100
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2131 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Thursday',
					start: '11:45',
					end: '13:15',
					group: 'V',
					room: 'D11',
					lecturer: 'Bx',
					enrolledStudents: [],
					maxParticipants: 100
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2132 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Thursday',
					start: '14:15',
					end: '15:45',
					group: 'C',
					room: 'D15',
					lecturer: 'Bx',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Thursday',
					start: '14:15',
					end: '15:45',
					group: 'D',
					room: 'D13',
					lecturer: 'SM/Wl',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Thursday',
					start: '16:00',
					end: '17:30',
					group: 'A',
					room: 'D13',
					lecturer: 'SM/Wl',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Thursday',
					start: '16:00',
					end: '17:30',
					group: 'B',
					room: 'D15',
					lecturer: 'Bx',
					enrolledStudents: [],
					maxParticipants: 20
				},
				{
					weekday: 'Thursday',
					start: '17:45',
					end: '19:15',
					group: 'E',
					room: 'D13',
					lecturer: 'Bx',
					enrolledStudents: [],
					maxParticipants: 20
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2111 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Friday',
					start: '08:15',
					end: '09:45',
					group: 'V',
					room: 'D11',
					lecturer: 'Sk',
					enrolledStudents: [],
					maxParticipants: 100
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2461 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Friday',
					start: '14:30',
					end: '16:00',
					group: 'V',
					room: 'D11',
					lecturer: 'Br',
					enrolledStudents: [],
					maxParticipants: 100
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})

	Subject.findOneAndUpdate({ courseNumber: 2462 }, {
		$push: {
			schedule: {
				$each: [{
					weekday: 'Friday',
					start: '16:15',
					end: '17:45',
					group: 'I',
					room: 'D11',
					lecturer: 'Br',
					enrolledStudents: [],
					maxParticipants: 50
				},
				{
					weekday: 'Friday',
					start: '17:45',
					end: '19:15',
					group: 'II',
					room: 'D11',
					lecturer: 'Br',
					enrolledStudents: [],
					maxParticipants: 50
				}]
			}
		}
	}, (err, subject) => {
		if (err) console.error(err)
	})
}
