// Filtere alle Module, die zu der PO eines Studenten gehören.
module.exports = async function (student, mongoose) {
	let k = await student.populate({
		path: 'examReg',
		populate: {
			path: 'subjects.id',
			options: {
				new: true
			}
		},
		options: {
			new: true
		}
	}).execPopulate()

	return k
}
