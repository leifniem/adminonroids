// Berechne, welche Module von einem Studenten belegt werden können.
module.exports = function (relevantModules, passedModules) {
	let moduleLookAhead = 2

	let semesterOfLowestUnfinishedSubject = Math.min.apply(Math, relevantModules.map((subject) => {
		if (passedModules.indexOf(subject.id) === -1) {
			return subject.destinedForSemester
		}
	}))
	let availableSubjects = relevantModules.filter((subject) => {
		let isIdNotNull = subject.id !== null
		let isInLookAheadRange = (semesterOfLowestUnfinishedSubject + moduleLookAhead) >= subject.destinedForSemester
		return isInLookAheadRange && isIdNotNull
	})
	return availableSubjects
}
