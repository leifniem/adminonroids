// Berechne Prioritätscore für einen Studenten und ein Fach.
// Niedriger Score bedeutet höhere Priorität
module.exports = function (student, examReg, subject) {
	// Gewichte, die angepasst werden können
	const creditPointWeight = 30
	const sameSemesterWeight = 1
	const nthChoiceWeight = 1

	let destinedForSemester
	for (const subj of examReg.subjects) {
		if (subject._id.equals(subj.id)) {
			destinedForSemester = subj.destinedForSemester
			break
		}
	}

	let choiceCoefficient = 1 / nthChoiceWeight
	let creditPointCoefficient = student.creditPoints / creditPointWeight
	let sameSemesterCoefficient = sameSemesterWeight * Math.max((student.semestersInMajor - destinedForSemester), 0)
	let score = choiceCoefficient * (sameSemesterCoefficient - creditPointCoefficient)
	return score
}
