// Filtere schon volle Module heraus.
module.exports = function (availableSubjects, mongoose) {
	availableSubjects.map((subject) => {
		let isFull = false
		subject.id.schedule.forEach(activity => {
			if (activity.enrolledStudents.length >= activity.maxParticipants) {
				isFull = true
			} else {
				isFull = false // there are groups which aren't full
			}
		})
		if (!isFull) { return subject }
	})

	return availableSubjects
}
