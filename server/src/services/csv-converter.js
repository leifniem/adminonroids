import path from 'path'
import csvtojson from 'csvtojson'

const translateDict = {
	pversion: 'specExamReg',
	CP: 'creditPoints',
	part: 'achievementType',
	pltxt1: 'title',
	pfsem: 'destinedForSemester',
	pordnr: 'courseNumberInExamReg',
	pnr: 'courseNumber',
	pktxt: 'titleAbbreviation',
	stg: 'major'
}

const semesterCountDict = {
	2013: 6,
	2015: 6,
	2017: 7,
	2016: 4
}

const majorDict = {
	IDB: 'Medieninformatik Bachelor',
	IMB: 'Medieninformatik Master'
}

const moduleFromAchievementType = {
	'PL': 'V',
	'SL': 'P'
}

// Lies csv Datei ein und erzeuge Datenbankobjekte (Studiengänge, POs und Module)
export default async function (mongoose) {
	const ExamReg = mongoose.model('ExamReg')
	const Subject = mongoose.model('Subject')
	const Major = mongoose.model('Major')
	ExamReg.remove({}, (err, removed) => { if (err) console.error(err) })
	Subject.remove({}, (err, removed) => { if (err) console.error(err) })
	Major.remove({}, (err, removed) => { if (err) console.error(err) })

	let jsonObjs = await csvtojson().fromFile(path.resolve(__dirname, '../../../anforderungs-dokumente/gehl-daten/pord_grunddaten.csv'))
	let subjects = {}
	for (let entry of jsonObjs) {
		if (entry.part === 'PL' || entry.part === 'SL') {
			for (const key in translateDict) {
				entry[translateDict[key]] = entry[key]
				delete entry[key]
			}
			let number = entry.courseNumber
			delete entry.courseNumber

			let specExamReg = {
				name: parseInt(entry.specExamReg),
				destinedForSemester: entry.destinedForSemester,
				creditPoints: entry.creditPoints,
				courseNumberInExamReg: entry.courseNumberInExamReg,
				major: entry.major
			}

			if (!subjects[number]) {
				subjects[number] = {
					title: entry.title,
					titleAbbreviation: entry.titleAbbreviation,
					achievementType: entry.achievementType,
					specExamRegs: []
				}
			}
			if (entry.title !== subjects[number].title) specExamReg['titleOverride'] = entry.title
			subjects[number].specExamRegs.push(specExamReg)
		}
	}

	let majors = {}
	let examRegs = {}

	Object.keys(subjects).forEach(async (key) => {
		let mainSubject = new Subject({
			title: subjects[key].title,
			titleAbbreviation: subjects[key].titleAbbreviation,
			courseNumber: key,
			moduleType: moduleFromAchievementType[subjects[key].achievementType],
			achievementType: subjects[key].achievementType
		})

		mainSubject.save()
			.catch((err) => console.error(err))

		Object.keys(subjects[key].specExamRegs).forEach((poNumb) => {
			let po = subjects[key].specExamRegs[poNumb]

			if (examRegs[po.name]) {
				examRegs[po.name].subjects.push({
					id: mainSubject._id,
					destinedForSemester: po.destinedForSemester,
					creditPoints: po.creditPoints,
					courseNumberInExamReg: po.courseNumberInExamReg
				})
			} else {
				examRegs[po.name] = {
					name: po.name,
					regularDestinedSemesters: semesterCountDict[po.name],
					major: po.major,
					subjects: [{
						id: mainSubject._id,
						destinedForSemester: po.destinedForSemester,
						creditPoints: po.creditPoints,
						courseNumberInExamReg: po.courseNumberInExamReg
					}]
				}
			}

			if (!majors[po.major]) {
				majors[po.major] = {
					abbreviation: po.major,
					name: majorDict[po.major],
					examRegulations: []
				}
			}
		})
	}, (err) => {
		if (err) console.error(err.errmsg)
	})

	Object.keys(examRegs).forEach(async (exReg) => {
		let maj = examRegs[exReg].major
		delete examRegs[exReg].major
		let ex = new ExamReg(examRegs[exReg])
		ex.save().catch((err) => {
			if (err) console.error(err.errmsg)
		})
		majors[maj].examRegulations.push(ex._id)
	})

	Object.keys(majors).forEach((maj) => {
		let major = new Major({
			...majors[maj]
		})
		major.save().catch((err) => {
			if (err) console.error(err.errmsg)
		})
	})
}
