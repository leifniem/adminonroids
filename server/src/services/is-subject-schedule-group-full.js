module.exports = async function (dataDTO, mongoose) {
	const Subject = mongoose.model('Subject')

	let subjectId = dataDTO.subjectId
	let group = dataDTO.group

	let foundSubject = await Subject.findOne({
		_id: subjectId,
		schedule: {
			$elemMatch: { group: group }
		}
	})

	if (foundSubject) {
		for (let i = 0; i < foundSubject.schedule.length; i++) {
			let appointment = foundSubject.schedule[i]
			if (appointment.group === group) {
				return appointment.maxParticipants >= appointment.enrolledStudents.length
			}
		}
	}
	return false
}
