// dependencies
import 'babel-polyfill'
import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import cors from 'cors'
import bcrypt from 'bcrypt-nodejs'

// services
import progressService from './services/progress-regulation'
import filterFullSubjects from './services/filter-full-subjects'
import subjectFilter from './services/subject-filter'
import csvConverter from './services/csv-converter'
import enrollToSubject from './services/enroll-to-subject'
import createTestData from './services/create-test-data'
import isScheduleGroupFull from './services/is-subject-schedule-group-full'
import deselectFromSubject from './services/deselect-from-subject'

// import models once to load them
/* eslint-disable no-unused-vars */
import models from './models/'

const isDeveloping = process.env.NODE_ENV !== 'production'
const port = isDeveloping ? 1717 : process.env.PORT
const mongoUrl = isDeveloping ? 'mongodb://localhost:27017/AdminOnRoids' : 'mongodb://' + process.env.MONGODB_PORT_27017_TCP_ADDR + ':' + process.env.MONGODB_PORT_27017_TCP_PORT + '/AdminOnRoids'

const app = express()
app.disable('x-powered-by')

// add server extensions
app.use(bodyParser.json())
app.use(cors())

// server url - ip:port/database
const serverURL = 'mongodb://localhost:27017/AdminOnRoids'
mongoose.Promise = Promise
mongoose.connect(serverURL)
// mongoose.set('debug', true)

const db = mongoose.connection
db.on('error', (err) => {
	console.log(err)
})

// HTTP status codes
const CLIENT_BAD_REQUEST = 400
const CLIENT_UNAUTHORIZED = 401
const SERVER_INTERNAL_SERVER_ERROR = 500
const SUCCESS_OK = 200

const Subject = mongoose.model('Subject')
const Student = mongoose.model('Student')
const Major = mongoose.model('Major')
const ExamReg = mongoose.model('ExamReg')

// routes
app.all('/', (req, res) => {
	res.send('<a href="/gettestdata">gettestdata</a><br>' +
		'<a href="/gettestusers">gettestusers</a><br>' +
		'<a href="/createtestdata">createtestdata</a><br>' +
		'<a href="/runcsvconverter">runcsvconverter</a><br>' +
		'<a href="/students">students</a><br>' +
		'<a href="/examregs">examregs</a><br>' +
		'<a href="/subjects">subjects</a><br>' +
		'<a href="/majors">majors</a><br>')
})

app.all('/deselect-student-from-subject', async (req, res) => {
	let userId = req.body.userId
	let subjectId = req.body.subjectId
	let courseId = req.body.courseId
	let group = req.body.group
	let derollmentDTO = { userId: userId, subjectId: subjectId, group: group }

	if (!userId || !subjectId || !group) {
		res.status(CLIENT_BAD_REQUEST).send({ msg: 'UserId, subjectId and group(A..Z) needed' })
		return
	}
	await deselectFromSubject(derollmentDTO, mongoose)
	Subject.findById(subjectId, (error, found) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send({ msg: 'couldn\'t get the subject' })
			return
		}
		Student.findById(userId, (error, user) => {
			if (error) {
				res.status(SERVER_INTERNAL_SERVER_ERROR).send({ msg: 'couldn\'t get student' })
				return
			}
			/* eslint-disable */
			user.enrolledToModules = user.enrolledToModules.filter(modul => modul.subject != subjectId && modul.course != courseId)
			/* eslint-enable */
			user.save().catch((err) => {
				if (err) console.error(err)
			})
			subjectFilter(user, mongoose).then((result) => {
				result = result.toObject()
				result.examReg.subjects = progressService(result.examReg.subjects, result.passedModules)
				return res.status(SUCCESS_OK).json(result)
			})
		})
	})
})

app.all('/enroll-student-to-subject', async (req, res) => {
	let userId = req.body.userId
	let subjectId = req.body.subjectId
	let courseId = req.body.courseId
	let group = req.body.group

	if (!userId || !subjectId || !group) {
		res.status(CLIENT_BAD_REQUEST).send({ msg: 'UserId, subjectId and group(A..Z) needed' })
		return
	}

	let enrollmentDTO = { userId: userId, subjectId: subjectId, group: group }
	let isSelectedGroupNotFull = await isScheduleGroupFull(enrollmentDTO, mongoose)

	if (isSelectedGroupNotFull) {
		Subject.findById(subjectId, (error, found) => {
			if (error) {
				res.status(SERVER_INTERNAL_SERVER_ERROR).send({ msg: 'couldn\'t get subject' })
				return
			}
			enrollToSubject(enrollmentDTO, mongoose)
			Student.findById(userId, (error, user) => {
				if (error) {
					res.status(SERVER_INTERNAL_SERVER_ERROR).send({ msg: 'couldn\'t get student' })
					return
				}
				user.enrolledToModules.push({ subject: subjectId, course: courseId })
				user.save().catch((err) => {
					if (err) console.error(err)
				})
				subjectFilter(user, mongoose).then((result) => {
					result = result.toObject()
					result.examReg.subjects = progressService(result.examReg.subjects, result.passedModules)
					return res.status(SUCCESS_OK).json(result)
				})
			})
		})
	} else { res.status(SERVER_INTERNAL_SERVER_ERROR).send({ msg: 'group is propably full' }) }
})

app.get('/filter-full-subjects', (req, res) => {
	Student.findOne({})
		.exec((error, student) => {
			if (error) console.error(error)
			subjectFilter(student, mongoose).then((result) => {
				let availableModules = progressService(result.examReg.subjects, student.passedModules)
				res.json(filterFullSubjects(availableModules, mongoose))
			})
		})
})

app.all('/login', (req, res) => {
	let username = req.body.username
	let password = req.body.password

	Student.findOne({ username: username })
		.exec((err, user) => {
			if (err) {
				return res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			} else if (!user || !(bcrypt.compareSync(password, user.password))) {
				return res.status(CLIENT_UNAUTHORIZED).json({ msg: 'Invalid user data' })
			} else {
				subjectFilter(user, mongoose).then((result) => {
					result = result.toObject()
					result.examReg.subjects = progressService(result.examReg.subjects, result.passedModules)
					return res.status(SUCCESS_OK).json(result)
				})
			}
		})
})

app.all('/chooseCourses', (req, res) => {
	let courses = req.body.courses
	// TODO Kurse wählen oder Fehler zurückgeben
	return res.status(SUCCESS_OK).json(courses)
})

app.get('/gettestdata', (req, res) => {
	ExamReg.find({}, (err, found) => {
		if (err) console.error(err)
		res.jsonp(found)
	})
})

app.get('/testpopulate', async (req, res) => {
	let user = await Student.findOne({})
	// subjectFilter(user, mongoose).then((result) => {
	// 	res.json(result)
	// })
	subjectFilter(user, mongoose).then(result => res.json(result))
})

app.get('/gettestusers', (req, res) => {
	Student.find({}, (err, found) => {
		if (err) console.error(err)
		res.jsonp(found)
	})
})

app.get('/createtestdata', (req, res) => {
	createTestData(mongoose).then(
		res.send('Test data created.')
	)
})

app.get('/runcsvconverter', (req, res) => {
	csvConverter(mongoose)
	res.send('Imported csv data.')
})

// RESTful paths for students, subjects, majors, examregs beginning here:
// get all students
app.get('/students', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	Student.find({}, (error, students) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send({
			students: students
		})
	})
})

// get student by id
app.get('/students/:mtrnum', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	Student.findOne({ matriculationNumber: req.params.mtrnum }, (error, student) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send(student)
	})
})

// create new student
app.post('/students', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	let newStud = new Student(req.body)
	newStud.save((err) => {
		if (err) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send('New student created.')
	})
})

// get all subjects
app.get('/subjects', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}
	Subject.find({}, (error, subjects) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send({
			subjects: subjects
		})
	})
})

// get subject by id
app.get('/subjects/:crsnum', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	Subject.findOne({ courseNumber: req.params.crsnum }, (error, subject) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send(subject)
	})
})

// create new subject
app.post('/subjects', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	let newSubj = new Subject(req.body)
	newSubj.save((error) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send('New subject created.')
	})
})

// get all examination regulations
app.get('/examregs', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	ExamReg.find({}, (error, examregs) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send({
			examregs: examregs
		})
	})
})

// get examination regulation by id
app.get('/examregs/:name', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	ExamReg.findOne({ name: req.params.name }, (error, examreg) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send(examreg)
	})
})

// create new examination regulation
app.post('/examregs', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	let newExamReg = new ExamReg(req.body)
	newExamReg.save((err) => {
		if (err) console.error(err)
		res.send('New examination regulation created.')
	})
})

// get all majors
app.get('/majors', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	Major.find({}, (error, majors) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send({
			majors: majors
		})
	})
})

// get major by id
app.get('/majors/:abbrv', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	Major.findOne({ abbreviation: req.params.abbrv }, (error, major) => {
		if (error) {
			res.status(SERVER_INTERNAL_SERVER_ERROR).send()
			return
		}
		res.send(major)
	})
})

// create new major
app.post('/majors', (req, res) => {
	if (!req.body.matnr) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'For logged in users only' })
		return
	}

	if (!req.body.isAdmin) {
		res.status(CLIENT_UNAUTHORIZED).send({ msg: 'Students do not have access to this feature' })
		return
	}

	let newMajor = new Major(req.body)
	newMajor.save((err) => {
		if (err) console.error(err)
		res.send('New major created.')
	})
})

// expose server on port
console.log('Server listening on Port ' + port)
app.listen(port)
