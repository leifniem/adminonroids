import assert from 'assert'
import mongoose from 'mongoose'
import progressRegulation from '../src/services/progress-regulation.js'

const Student = mongoose.model('Student')
const ExamReg = mongoose.model('ExamReg')

describe('progress regulation tests', () => {
	it('subject from fourth semester with no passed modules', (done) => {
		Student.findOne({ username: 'mastaige' }, (err, student) => {
			if (err) console.error(err)
			ExamReg.findOne({ _id: student.examReg }, (err, examReg) => {
				if (err) console.error(err)
				console.log('Ergebnis: ' + progressRegulation(examReg.subjects, []))
				assert.equal(progressRegulation(examReg.subjects, []).length !== 0, 'subject available but should not')
				done()
			})
		})
	})
})
