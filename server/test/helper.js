import mongoose from 'mongoose'

require('../src/models')

// const Student = mongoose.model('Student')
// const ExamReg = mongoose.model('ExamReg')
// const Subject = mongoose.model('Subject')

// ExamReg.remove({}, (error, removed) => { if (error) console.log(error) })
// Subject.remove({}, (error, removed) => { if (error) console.log(error) })
// Student.remove({}, (error, removed) => { if (error) console.log(error) })

before((done) => {
	mongoose.connect('mongodb://localhost:27017/AdminOnRoids')
	mongoose.set('debug', false)
	const db = mongoose.connection
	db.on('error', console.error.bind(console, 'connection error'))
	db.once('open', function () {
		console.log('connected to database\n')
		done()
	})
})

after(function (done) {
	mongoose.connection.close(done)
})
