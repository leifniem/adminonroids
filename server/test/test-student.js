import assert from 'assert'
import { compareSync, hashSync } from 'bcrypt-nodejs'
import mongoose from 'mongoose'

const Student = mongoose.model('Student')

describe('student tests', () => {
	it('find student', (done) => {
		Student.find({ username: 'mastaige' }, (err, student) => {
			if (err) console.log(err)
			assert(student.length !== 0, 'user not found')
			done()
		})
	})

	it('student not found', (done) => {
		Student.find({ username: 'unknown user' }, (err, student) => {
			if (err) console.log(err)
			assert(student.length === 0, 'user found but should not')
			done()
		})
	})

	it('wrong password', (done) => {
		Student.findOne({ username: 'mastaige' }, (err, user) => {
			if (err) console.log(err)
			assert(!compareSync(hashSync('wrong'), user.password), 'accepted wrong password as correct')
			done()
		})
	})

	it('login', (done) => {
		Student.findOne({ username: 'mastaige' }, (err, user) => {
			if (err) console.log(err)
			assert(compareSync(hashSync('bla'), user.password), 'wrong username or password')
			done()
		})
	})
})
