import assert from 'assert'
import mongoose from 'mongoose'
import assignmentCalculator from '../src/services/assignment-calculator.js'

const Student = mongoose.model('Student')
const ExamReg = mongoose.model('ExamReg')
const Subject = mongoose.model('Subject')

describe('assignement calculator tests', (done) => {
	it('90cp but one semsester late  == 60cp but right semester', () => {
		Student.findOne({ username: 'justaige' }, (err, studOne) => {
			if (err) console.error(err)
			Student.findOne({ username: 'mastaige' }, (err, studTwo) => {
				if (err) console.error(err)
				ExamReg.findOne({ _id: studOne.examReg }, (err, examReg) => {
					if (err) console.error(err)
					Subject.findOne({ courseNumber: 4121 }, (err, subject) => {
						if (err) console.error(err)
						assert.equal(assignmentCalculator(studOne, examReg, subject), assignmentCalculator(studTwo, examReg, subject), 'score not equal')
						done()
					})
				})
			})
		})
	})
})
