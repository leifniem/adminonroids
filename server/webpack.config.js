const path = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const env = process.env.NODE_ENV === 'production'

let config = {
	entry: [
		'babel-polyfill',
		'./src/index.js'
	],
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	mode: env ? 'production' : 'development',
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.js?$/,
				loader: 'eslint-loader',
				exclude: [path.resolve(__dirname, 'node_modules')]
			},
			{
				test: /\.js?$/,
				loader: 'babel-loader',
				options: {
					presets: ['minify']
				},
				exclude: [path.resolve(__dirname, 'node_modules')]
			}
		]
	},
	devServer: {
		contentBase: path.resolve(__dirname, 'dist'),
		compress: true,
		hot: true,
		https: false,
		host: 'localhost',
		proxy: {
			'^/*': {
				target: 'http://localhost:1717/'
			}
		}
	},
	externals: [nodeExternals()],
	plugins: [],
	target: 'node',
	resolve: {
		extensions: ['.js', '.json', '.node']
	}
}

if (!env) {
	config.plugins.push(new webpack.DefinePlugin({'process.env.NODE_ENV': JSON.stringify('development')}))
	config.plugins.push(new webpack.NamedModulesPlugin())
	config.plugins.push(new webpack.HotModuleReplacementPlugin())
} else {
	delete config.devServer
	config.plugins.push(new webpack.DefinePlugin({'process.env.NODE_ENV': JSON.stringify('production')}))
}

module.exports = config
