# AdminOnRoids

Leif Niemczik
Laszlo von Kacsoh
Vincent Mathis
Maximilian Staiger

## Installation

Installieren Sie node.js / npm falls nicht vorhanden.
Klonen Sie das Repository (https://bitbucket.org/leifniem/adminonroids/src/master/) mithilfe von git.
Anschließend führen Sie diese Befehle aus:

```
cd server
npm i

cd ../client
npm i
```

## Server und Client starten

Um Server und Client nebenläufig zu starten, wechseln Sie zum Root des Repository und benutzen Sie:

```
npm run dev
```
Alternativ können Sie Client und Server auch seperat starten, führen sie ```npm run dev``` dazu im jeweilgen Ordner ```client``` oder ```server``` aus.

## Testdaten erzeugen

Wechseln Sie auf

```
localhost:1717
```

Klicken Sie auf 
```
runcsvconverter
```
Anschließend gehen Sie zurück und klicken auf
```
createtestdata
```

## Client öffnen

Der client ist erreichbar unter:

```
localhost:8080
```