const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MinifyPlugin = require('babel-minify-webpack-plugin')

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	mode: 'production',
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.js?$/,
				loader: 'eslint-loader',
				exclude: [path.resolve(__dirname, 'node_modules')]
			},
			{
				test: /\.js?$/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env'],
					plugins: [require('@babel/plugin-proposal-object-rest-spread')]
				},
				exclude: [path.resolve(__dirname, 'node_modules')]
			},
			{
				test: /\.vue?$/,
				loader: 'vue-loader'
			},
			{
				test: /\.(s)?css$/,
				use: [
					'vue-style-loader',
					'css-loader',
					'sass-loader'
				]
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html',
			title: 'Admin on Roids'
		}),
		new MinifyPlugin()
	],
	target: 'web',
	resolve: {
		alias: {
			'@': path.join(__dirname, './src/'),
			'vue$': 'vue/dist/vue.esm.js'
		},
		extensions: ['.js', '.vue', '.json', '.css', '.node']
	}
}
