const path = require('path')
const webpack = require('webpack')

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	mode: 'development',
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.js?$/,
				loader: 'eslint-loader',
				exclude: [path.resolve(__dirname, 'node_modules')]
			},
			{
				test: /\.js?$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						plugins: [require('@babel/plugin-proposal-object-rest-spread')],
					}
				},
				exclude: [path.resolve(__dirname, 'node_modules')]
			},
			{
				test: /\.vue?$/,
				loader: 'vue-loader'
			},
			{
				test: /\.(png|jp(e*)g|svg)$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[path][name].[hash].[ext]'
					}
				}]
			}
		]
	},
	devServer: {
		contentBase: path.resolve(__dirname, 'dist'),
		compress: true,
		hot: true,
		https: false,
		host: 'localhost'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.SourceMapDevToolPlugin()
	],
	target: 'web',
	resolve: {
		alias: {
			'@': path.join(__dirname, './src/'),
			vue$: 'vue/dist/vue.esm.js'
		},
		extensions: ['.js', '.vue', '.json', '.css', '.node']
	}
}
