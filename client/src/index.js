import Vue from 'vue'
import axios from 'axios'
import VueRouter from 'vue-router'

import App from '@/App'
import Login from '@/components/Login'
import Timetable from '@/components/Timetable'
import ModuleChoice from '@/components/ModuleChoice'
import Schedule from '@/components/Schedule'
import {store} from './store'

Vue.use(VueRouter)

const router = new VueRouter({
	saveScrollPosition: true,
	history: true,
	routes: [
		{
			path: '/',
			component: Login
		},
		{
			path: '/modulechoice',
			component: ModuleChoice
		},
		{
			path: '/timetable',
			component: Timetable
		},
		{
			path: '/schedule',
			component: Schedule
		}
	]
})

router.beforeEach((to, from, next) => {
	if (store.state.isLoggedIn && to.path === '/') {
		next('/timetable')
	} else if (!store.state.isLoggedIn && to.path !== '/') {
		next('/')
	} else {
		next()
	}
})

Vue.http = Vue.prototype.$http = axios

/* eslint-disable no-new */
new Vue({
	components: { App },
	store,
	router,
	template: '<App/>'
}).$mount('#app')
