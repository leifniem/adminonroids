import axios from 'axios'
import Vuex from 'vuex'
import Vue from 'vue'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const server = axios.create({ baseURL: 'http://localhost:1717' })

export const store = new Vuex.Store({
	state: {
		student: null,
		isLoggedIn: localStorage.getItem('user-token'),
		chosenModules: [],
		chosenCourses: [],
		shownSemester: 'alle',
		errMsg: ''
	},
	plugins: [createPersistedState()],
	mutations: {
		setStudent (state, input) {
			state.student = input
			state.isLoggedIn = true
		},
		removeStudent: (state) => {
			state.student = null
			state.isLoggedIn = false
			state.chosenModules = []
			state.chosenCourses = []
			state.shownSemester = 'alle'
		},
		chooseModules: (state, input) => {
			state.chosenModules = input
		},
		chooseCourses: (state, input) => {
			state.chosenCourses = input
		},
		changeSemester: (state, input) => {
			state.shownSemester = input
		},
		setErrMsg: (state, input) => {
			state.errMsg = input
		}
	},
	getters: {
		student: state => { return state.student },
		isLoggedIn: state => { return state.isLoggedIn },
		chosenModules: state => { return state.chosenModules },
		chosenCourses: state => { return state.chosenCourses },
		shownSemester: state => { return state.shownSemester },
		errMsg: state => { return state.errMsg }
	},
	actions: {
		handleLogin ({commit}, data) {
			return new Promise((resolve, reject) => {
				server
					.post('/login', {
						'username': data.username,
						'password': data.password
					})
					.then(response => {
						localStorage.setItem('user-token', true)
						commit('setStudent', response.data)
						commit('setErrMsg', '')
						resolve({success: true})
					}, error => {
						reject(error)
						commit('setErrMsg', error.response.data.msg)
					})
			})
		},
		handleLogout ({commit}) {
			return new Promise((resolve, reject) => {
				localStorage.removeItem('user-token')
				commit('removeStudent')
				resolve()
			}, error => {
				console.error(error)
			})
		},
		chooseModules ({commit}, data) {
			return new Promise((resolve, reject) => {
				commit('chooseModules', data.modules)
				resolve()
			}, error => {
				console.error(error)
			})
		},
		chooseCourses ({commit}, data) {
			return new Promise((resolve, reject) => {
				commit('chooseCourses', data.courses)
				resolve()
			}, error => {
				console.error(error)
			})
		},
		enrollStudent ({commit}, data) {
			return new Promise((resolve, reject) => {
				server
					.post('/enroll-student-to-subject', {
						'userId': this.state.student.id,
						'subjectId': data.subjectId,
						'courseId': data.courseId,
						'group': data.group
					})
					.then(response => {
						commit('setStudent', response.data)
						console.log(response.data)
						resolve({success: true})
					}, error => {
						reject(error)
						commit('setErrMsg', error.response.data.msg)
					})
			})
		},
		deselectStudent ({commit}, data) {
			return new Promise((resolve, reject) => {
				server
					.post('/deselect-student-from-subject', {
						'userId': this.state.student.id,
						'subjectId': data.subjectId,
						'courseId': data.courseId,
						'group': data.group
					})
					.then(response => {
						commit('setStudent', response.data)
						resolve({success: true})
					}, error => {
						reject(error)
						commit('setErrMsg', error.response.data.msg)
					})
			})
		},
		changeSemester ({commit}, data) {
			return new Promise((resolve, reject) => {
				commit('changeSemester', data.semester)
				resolve()
			}, error => {
				console.error(error)
			})
		}
	}
})
