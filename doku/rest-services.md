# Admin on Roids - REST API

## Einen Studenten aus einer Kursgruppe entfernen

Antwortet mit dem entfernten Studenten

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/deselect-student-from-subject`
__Method__ | GET, POST
__URL Params__ | -
__Data Params__ | userId, subjectId, group

### Success Response

```javascript
Code: 200 - OK
Content: { user: user }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 400 - BAD REQUEST
Content: { msg: 'UserId, subjectId and group(A..Z) needed' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/deselect-student-from-subject', {
    userId: 123,
    subjectId: 456,
    group: A
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Einen Studenten in eine Kursgruppe hinzufügen

Antwortet mit dem hinzugefügten Studenten

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/enroll-student-to-subject`
__Method__ | GET, POST
__URL Params__ | -
__Data Params__ | userId, subjectId, group

### Success Response

```javascript
Code: 200 - OK
Content: { user: user }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 400 - BAD REQUEST
Content: { msg: 'UserId, subjectId and group(A..Z) needed' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/enroll-student-to-subject', {
    userId: 123,
    subjectId: 456,
    group: A
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Login

Antwortet mit allen Modulen die der Student belegen darf.
Zusätzlich erhält der Nutzer zugriff aufs System.

### HTTP Info

__Header__ | login
|------|--------|
__URL__ | `/subject-filter`
__Method__ | GET, POST
__URL Params__ | -
__Data Params__ | username, password

### Success Response

```javascript
Code: 200 - OK
Content: { subjects }
```

### Error Response


```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Invalid user data' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/login', {
    username: joebiff,
    password: //some hash
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Alle Studenten abgreifen

Antwortet mit einer Liste von allen Studenten.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/students`
__Method__ | GET
__URL Params__ | -
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { students: students }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/students').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Einen Studenten abgreifen

Antwortet mit einem JSON, welches die Studentendaten beinhaltet

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/students/:mtrnum`
__Method__ | GET
__URL Params__ | Matrikelnummer des gesuchten Studenten
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { student }
```

> Oder

```javascript
Code: 200 - OK
Content: {}
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/students/17426900').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Einen Studenten anlegen

Erstellt einen neuen Studenten in der Datenbank.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/students`
__Method__ | POST
__URL Params__ | -
__Data Params__ | json with personel data

### Success Response

```javascript
Code: 200 - OK
Content: { 'New student created.' }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.post('/students', {
    // data like name, date of birth and so on
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

### Notes

- Die Studenten-Email wird automatisch generiert
- Der Username wird automatisch generiert und ggf mit *'x'* aufgefüllt, falls zu kurz

## Alle Module abgreifen

Liefert alle in der Datenbank hinterlegten Module

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/subjects`
__Method__ | GET
__URL Params__ | -
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { 'New student created.' }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/subjects').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Ein Modul abgreifen

Liefert ein einzelnes Modul

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/subjects:crsnum`
__Method__ | GET
__URL Params__ | subjectId
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { 'New subject created.' }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/subjects/10101010101110').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Ein Modul anlegen

Legt ein neues Modul in der Datenbank an.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/subjects`
__Method__ | POST
__URL Params__ | -
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { subject: subject }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.post('/subjects', {
    // subject informations
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Alle Prüfungsordnungen abgreifen

Liefert alle Prüfungsordnungen aus der Datenbank.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/examregs`
__Method__ | GET
__URL Params__ | -
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { examregs: examregs }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/examregs').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Eine Prüfungsordnung abgreifen

Liefert eine Prüfungsordnung aus der Datenbank.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/examregs/:name`
__Method__ | GET
__URL Params__ | name der PO
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { examregs: examregs }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/examregs/po2015').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Eine Prüfungsordnung anlegen

Liefert eine Prüfungsordnung aus der Datenbank.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/examregs`
__Method__ | POST
__URL Params__ | -
__Data Params__ | PO objekt

### Success Response

```javascript
Code: 200 - OK
Content: { 'New examination regulation created.' }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.post('/examregs', {
    // examination regulation informations
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Alle Studiengänge abgreifen

Liefert alle Studiengänge aus der Datenbank.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/majors`
__Method__ | GET
__URL Params__ | -
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { majors: majors }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/majors').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Einen Studiengang abgreifen

Liefert eine Prüfungsordnung aus der Datenbank.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/majors/:abbrv`
__Method__ | GET
__URL Params__ | Abkürzung des Studiengangs
__Data Params__ | -

### Success Response

```javascript
Code: 200 - OK
Content: { major: major }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.get('/majors/medinf').then(response => {
    // do something with it
}, error => {
    // your error handling
})
```

## Einen Studiengang anlegen

Liefert einen neuen Studiengang in der Datenbank an.

### HTTP Info

__Header__ | Data
|------|--------|
__URL__ | `/majors`
__Method__ | POST
__URL Params__ | -
__Data Params__ | major objekt

### Success Response

```javascript
Code: 200 - OK
Content: { 'New major created.' }
```

### Error Response

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'For logged in users only' }
```

> Oder

```javascript
Code: 401 - UNAUTHORIZED
Content: { msg: 'Students do not have access to this feature' }
```

> Oder

```javascript
Code: 500 - INTERNAL SERVER ERROR
Content: {}
```

### Sample Call

```javascript
const server = axios.create({ baseURL: 'http://localhost:1717' })
server.post('/examregs', {
    // major informations
}).then(response => {
    // do something with it
}, error => {
    // your error handling
})
```
