# Admin on Roids - Architektonische Entscheidungen

In diesem Dokument wird offen gelegt, wie die Komponenten zusammenspielen sollen, und wie diese intern umgesetzt werden.
Zum Verständis fangen wir ganz hinten, bei der Datenbank, an und arbeiten uns zu dem vor was der Client sieht.
Wie in der Vorlesung besprochen, orientierten wir uns hauptsächlich am MEAN-Stack.
Abgewichen sind wir dabei nur insofern, dass wir Angular durch Vue.js ersetzt haben.

## Datenbank

Als Datenbank wird MongoDB eingesetzt.
Dies begründen wir mit Daten die Stark voneinander abhängig sind.
In der Unterpunkt "Hierachie" wird dies ersichtlicher.
Beim Konzipieren der Datenbank und der Sichtung der Beispieldaten von Frau Gehl ist uns klar geworden, dass wir eine Struktur finden müssen, welche den Sachverhalt aus der Realität am ehesten wiederspiegelt.

### Hierachie

Durch das übertragen von reellem Sachverhalt zu Datenbankkonzept wurde klarer, wie die Daten gehalten werden müssen.
>*Ein Studiengang hat keine bis viele Prüfungsordnungen.*

Demnach musste auch folgendes gelten:
>*Eine Prüfungsordnung hat keine bis viele Module, die angeboten werden können.*

Um Datendopplung zu vermeiden, entschieden wir uns an dieser Stelle die Module in einer Prüfungsordnung nur als Referenz zu halten.
Dadurch kann ein Modul nur einmal existieren, aber in beliebig vielen Prüfungsordnungen und somit auch in unterschiedlichen Studiengängen angeboten werden.

In MongoDB können Attribute eines Objekts auch Arrays von weiteren Objekten sein, wodurch diese Herachie problemlos umgesetzt werden kann.

Der folgende Ausschnitt zeigt eine etwaige Darstellung der Objekt-Hierachie.
Für eine genauere Beschreibung der einzelnen Objekte lesen Sie bitte das Kapittel *Objekt-Modells*.

```bash
Major
  |-ExamReg (Referenz)
       |-Subjects (Referenz)
             |-Subject (Referenz)
```

### Objekt-Modells

Für das proof-of-concept, das dieses Projekt darstellt, wurden nur vier entities bzw Datenbank Objekte benötigt.
Das nachfolgende Diagram zeigt, um Referenz-Pfeil-Chaos zu vermeiden, nur die einzelnen Objekte und ihre Attribute.
Beachten Sie dabei, dass nur die ersten Elemente der jeweiligen Reihe tatsächlich exisistieren.
Objekte wie *MetaSubject* wurden nur zur Übersichtlichkeit ausgelagert!

![Datenbank Entities][DatabaseObjects]

Mongo DataBase Models bringen neben der guten Lesbarkeit auch weitere Vorteile mit sich, wie zum Beispiel

> Ausführen von code vor dem Speichern eines Objektes.

```javascript
student.pre('save', function (next) {
    // generate username
    let first = this.firstName.substr(0, 2)
    let last = this.lastName
    while (last.length < 6) {
        last += 'x'
    }
    last = last.substr(0, 6)
    this.username = (first + last).toLowerCase()

    // generate email
    let secondName = ''
    if (this.firstName.includes(' ')) {
        secondName = this.firstName.split(' ')[1].substr(0, 1) + '.'
    }
    this.email = `${this.firstName.split(' ')[0]}.${secondName}${this.lastName}@student.hs-rm.de`.toLowerCase()

    next()
})
```

> per Regex prüfen ob der Wert überhaupt eingetragen werden darf.

```javascript
// Studiengang
major: {
    type: String,
    required: true,
    validate: {
        validator: function (v) {
            /^[A-Z]{3}$/.test(v)
        }
    }
},
```

Dies vereinfacht Serverseitig viele Aufgaben.
Bei einer Anfrage muss der Server selbst nicht mehr prüfen ob die übergebenen Daten überhaupt gültig sind, stattdessen kann die Datenbank diese Aufgabe selber erledigen und im Callback ggf. einen Error werfen.
Im folgenden Beispiel konnte in einem REST-Service auf einen Fehler mit entsprechender Meldung reagiert werden.

```javascript
Subject.findById(subjectId, (error, found) => {
    if (error) {
        res.status(SERVER_INTERNAL_SERVER_ERROR).send({ msg: 'couldn\'t get subject' })
        return
    }
```

### Ausblick

Von Haus aus arbeitet die gewählte Datenbank asynchron (in jeweils eigenen Prozessen) bzw. mit *promises* (Anfragen bei denen die Antwort *irgendwann* kommt), wodurch die Antworten meist nicht direkt kommen aber das System so lange nicht blockiert.
Es können also n-viele Anfragen gleichzeitig abgearbeitet werden.
Wir gehen davon aus, dass die Datenbank bei mehreren hundert gleichzeitigen Anfragen nicht in die Knie gehen wird.

## Server

Um die Performance weiter zu verbessern, wählten wir *NodeJS* als JavaScript basierter Server mit dem *express* Framework. An zusätzlicher Middelware nutzen wir mongoose zur Kommunikation mit MongoDB, bcrypt für Passwortverschlüsselung, bodyPaser um mit HTTP-POST-Anfragen umgehen zu können und CORS für cross-origin Anfragen.
Auch dieser Baustein ist von Haus aus asynchron, wodurch die Reaktionsgeschwindigkeit des Systems deutlich weiter verbessert wird.

Als zusätzliches Ziel haben wir uns gesetzt, dass serverseitige Funktionalitäten leicht auszutauschen sind, da sich einige Sachverhalte schnell ändern könnten.
Beispielsweise könnte der Belegungsalgorhytmus ausgetauscht werden wollen.
Ein mögliches Szenario wäre ein Wechsel von Windhundverfahren zu Forschrittspriorisierung.

### REST

> Für eine ausführliche Dokumentation der REST-Services lesen Sie bitte die dazu beiliegende Seperate Datei.

Das Einbringen eines REST-Services in NodeJS hat sich als sehr bequem erwiesen.
Eine *Route* (Adresse unter der ein Service erreichbar ist) kann schnell angelegt und getestet werden.
Ebenso einfach ist es dem Server mitzuteilen unter welcher Adresse der Service erreichbar ist:

```javascript
const STATUS_OK = 200
const STATUS_BAD_REQUEST = 400
const app = express()

app.get('/say-hello/:name', (request, response) => {
    let name = request.params.name
    let message = 'Hello '
    let status = STATUS_OK

    if (!name) {
        message = 'No name given!'
        status = STATUS_BAD_REQUEST
     } else {
        message += name
    }
     response.status(status).json({ message: message })
})
```

In diesem kleinen Beispiel würden GET-Anfragen auf die Adresse */say-hello/:name* mit einem entsprechenden JSON beantwortet werden.
Ein Aufruf mit */say-hello/Joendhard* wird also `{ message: 'Hello Joendhard' }` liefern, während der Aufruf von */say-hello/* den Fehlercode 400 mit der Nachricht `{ message: 'No name given!' }` erzeugt.

### Microservices

Wie bereits erwähnt, sollen wichtige Funktionalitäten möglichst granular und modular gestaltet werden.
Um das zu ereichen haben wir einige Services in eigene Dateien ausgelagert.
Dadurch können Module nach belieben Importiert und genutzt werden.

```javascript
import enrollToSubject from './services/enroll-to-subject'
```

Wie ein Student nun eingeschrieben wird interessiert uns demnach nur beim Import.
Hinter dem Service könnte nun das Windhundverfahren oder ein komplexer Algorhytmus stehen, im Aufruf ändert sich wenn überhaupt nur die Anzahl der entgegengenommenden Argumente.

```javascript
enrollToSubject(enrollmentDTO, mongoose) // Implementierung A
enrollToSubject(subjectId, studentId) // Implementierung B
```

Dadurch entsteht eine ähnliche Verhaltensweise wie durch das Nutzen von Interfaces in Java.

Im folgenden Beispiel sehen wir die Implementierung __A__ aus vorherigem code-snipped.
Falls sich die Datenbank ändern sollte, könnte man hier eine einfache, isolierte Anpassung vornehmen.

```javascript
Subject.findOneAndUpdate(
    {
        _id: subjectId,
        schedule: { $elemMatch: { group: group } }
    },
    {
        $push: { 'schedule.$.enrolledStudents': { student: studentId } }
    }
)
```

## Client

Wie bereits zuvor erwähnt, haben wir uns für das Rendern der HTML-Seiten gegen Angular und stattdessen für Vue.js entschieden.
Vue ist schneller, leichter zu lernen und der Aufbau eines Templates ist wesentlich nachvollziehbar.
Zudem können Templates andere Templates als Module importieren, was die logische Unterteilung wesentlich vereinfacht.

Zusätzlich benutzen wir für bessere HTTP requests die Middleware axios.

### Vue

Components sind die Module, welche sich in die Teile *template*, *script* und *style* unterteilen.
Unter *Template* wird der HTML-Code generiert, *Script* ist der Javascript-Teil, welcher für die Berechnungen zuständig ist und *Style* übernimmt CSS-Anweisungen in verschiedener Syntax wie zum Beispiel SCSS.
Der Script-Teil unterteilt sich nochmal in drei Teile.
Bei *data* werden Daten gehalten, die das Script schon hat, *computed* sind berechnete Daten und *methods* hält die für das Template verfügbaren Funktionen.

```bash
Index.js
App.js
store.js
components/
    |- <module>.js
        |- template
        |- script
            |- data
            |- computed
            |- methods
        |- style
```

Um Vue besser zu verstehen, beginnen wir mit einem kleinen Beispiel.
Der Einfachheithalber überspringen wir das Routing und widmen uns direkt den Templates.
Als Beispiel sollen uns zwei Templates dienen.
Einen als Aufrufer und eines als aufgerufenes Modul.
Dabei soll das Modul nur Daten zugespielt bekommen und mit diesen Daten arbeiten.
Wir erinnern uns dabei an die Microservices des Servers.

```html
<template>
    <div id="hello-world-app">
        <div class="greet-box" v-for="name in names"> {{ greet(name) }} </div>
    </div>
</template>

<script>
    export default {
        name: "Greetings",
        props: ["names"],
        methods: {
            greet: function (name) {
                return "Hello " + name
            }
        },
        data () {
            return {}
        }
    }
</script>

<style lang ="scss">
    $accent: rgb(246, 80, 80);

    #hello-world-app {
        .greet-box {
            background-color: $accent;
            color: white;
        }
    }
</style>
```

Das Modul nimmt ein Attribut `names` entgegen und baut daraus einen wrapper mit n-vielen Kontainern, in denen der aus der Methode `greet(name)` generierte Text stehen wird.

```html
<template>
    <Greetings :names="persons"/>
</template>

<script>
    export default {
        name: "Main",
        data () {
            return {
                persons: ["Joendhard", "Susi", "Rudi"]
            }
        }
    }
</script>

<style lang ="scss">
</style>
```

Das aufrufende Template nutzt das Modul sehr bequem, indem der Name als Tag verwendet und ein Attribut mitgegeben wird.
Als Ergebnis wird anschließend folgendes HTML ausgegeben:

```html
<!--- ... -->
    <div id="hello-world-app">
        <div class="greet-box"> Hello Joendhard </div>
        <div class="greet-box"> Hello Susi </div>
        <div class="greet-box"> Hello Rudi </div>
    </div>
<!--- ... -->
```

### Router

Der VueRouter ist ein Bestandteil des Vue core und dafür zuständig, zu regeln, welche Komponente auf angezeigt werden. Durch den Router ist es extrem einfach komplette Komponenten auszutauschen.

Die verschieden Routes werden mit den jeweiligen Komponenten verknüpft.

```javascript
const router = new VueRouter({
    saveScrollPosition: true,
    history: true,
    routes: [
        {
            path: '/',
            component: Login
        },
        {
            path: '/modulechoice',
            component: ModuleChoice
        },
        {
            path: '/timetable',
            component: Timetable
        },
        {
            path: '/schedule',
            component: Schedule
        }
    ]
})
```

Ein Wechsel der Route kann im Script-Teil der Vue-Komponenten wiefolgt angestoßen werden.

```javascript
this.$router.push('/neueRoute')
```

Vor jedem Wechsel einer Route wird überprüft, ob der Nutzer eingelogt ist. Wenn nicht, wird der Nutzer zurück auf die Startseite geführt.

```javascript
router.beforeEach((to, from, next) => {
    if (store.state.isLoggedIn && to.path === '/') {
        next('/timetable')
    } else if (!store.state.isLoggedIn && to.path !== '/') {
        next('/')
    } else {
        next()
    }
})
```

### Store

Der VueX Store ist für das State Management zuständig. Außerdem werden dort die Server-Anfragen via axios gesendet.

Alle Informationen, die komponentenübergreifend gebraucht werden, werden in einem State des VueX Store gespeichert.

```javascript
state: {
        student: null,
        isLoggedIn: localStorage.getItem('user-token'),
        chosenModules: [],
        chosenCourses: [],
        shownSemester: 'alle',
        errMsg: ' '
    }
```

student
> Der angemeldete Studenten samt aller später relevanten Informationen wie Prüfungsordnung und alle Fächerinformationen

isLoggedIn 
> Der user-token, der bestätigt, dass der Nutzer angemeldet ist und die Studentenrechte hat

chosenModules
> Die im ModuleChoice gewählten Module

chosenCourses
> Die Kurse, in die der Student eingeschrieben ist

shownSemester
> Das aktuell anzuzeigende Semester im Timetable

errMsg
> Fehlermeldung, die dem Nutzer angezeigt werden soll


Via axios wird auf den Server zugegriffen.

```javascript
axios.create({ baseURL: 'http://localhost:1717' })
```

Wie eine Server-Anfrage umgesetzt wird, betrachten wir am Beispiel des Logins.
Wir senden einen POST-Request an die '/login'-Route des Servers inklusive username und password im json-Format. Wird der Login vom Server bestätigt, setzen wird das user-token und speichern die Daten, die wir vom Server bekommen. Sollte der Server den Login ablehnen, wird von ihm eine Nachricht mit dem Grund der Ablehnung zurück zum Absender gesendet. Der Inhalt dieser Error-Response wird dem Nutzer dann angezeigt. Eine mögliche Antwort für den Login könnte zum Beispiel *'invalid user data'* sein.

```javascript
server
    .post('/login', {
        'username': data.username,
        'password': data.password
    })
    .then(response => {
        localStorage.setItem('user-token', true)
        commit('setStudent', response.data)
        commit('setErrMsg', ' ')
        resolve({success: true})
    }, error => {
        reject(error)
        commit('setErrMsg', error.response.data.msg)
    })
```

## Weiterführendes

Für weitere Informationen besuchen Sie die offizielle Dokumentationen oder lesen unsere Installationsanweisung.

### Links

- [JavaScript ECMA 2018](http://www.ecma-international.org/publications/standards/Ecma-262.htm "Latest JavaScript documentation") - Latest JavaScript documentation
- [MongoDB](https://docs.mongodb.com/ "MongoDB documentation") - MongoDB documentation
- [Mongoose](http://mongoosejs.com/docs/ "Mongoose middleware documentation") - MongoDB middleware für NodeJS
- [NodeJs](https://nodejs.org/en/docs/ "NodeJS documentation") - NodeJS documentation
- [Vue.js](https://vuejs.org/v2/guide/ "Vue.js getting started") - Vue.js getting started
- [VueX](https://vuex.vuejs.org/guide/ "VueX getting started") - VueX getting started

[DatabaseObjects]: resources/DatabaseEnteties.png "Database Objects"